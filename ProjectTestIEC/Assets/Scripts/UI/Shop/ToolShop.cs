﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolShop : MonoBehaviour
{
    private bool _isDrag;
    private int numPress;
    [SerializeField] private GameObject panelTool;
    private const int cheatCoins = 1000;
    private const int cheatPremium = 1000;

    public void OnMouseDownLeftBottom() {
        _isDrag = true;
        numPress = 0;
    }
    public void OnMouseUpLeftBottom() { 
        _isDrag = false;
        numPress = 0;
    }
    public void OnButtonRight() {
#if UNITY_EDITOR
        Debug.Log("Editor");
        panelTool.SetActive(true);
        return ;
#endif
        if (_isDrag) {
            numPress++;
            if(numPress == 4) {
               panelTool.SetActive(true);
                numPress = 0;
                _isDrag = false;
            }
        }
    }
    public void OnButtonExit() {
        panelTool.SetActive(false);
    }
    public void OnButtonCheatCoin() {
        PlayerData.instance.coins += cheatCoins; 
        PlayerData.instance.Save();
    }
    public void OnButtonCheatPrenium() {
        PlayerData.instance.premium += cheatPremium;
        PlayerData.instance.Save();

    }
}
