﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShopItemListItem : MonoBehaviour
{
    public Image icon;
    public Text nameText;
    public Text pricetext;
	public Text premiumText;
    public Button buyButton;

	public Text countText;

	public Sprite buyButtonSprite;
	public Sprite disabledButtonSprite;
    public GameObject particleBuy;
    public void OnButtonBuy() {
        var par= Instantiate(particleBuy);
        par.transform.SetParent(icon.transform);
        par.transform.position = icon.transform.position;

        DOVirtual.DelayedCall(0.5f, () => Destroy(par));
    }
}
